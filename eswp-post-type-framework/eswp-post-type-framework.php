<?php
/*
Plugin Name: Eastern Standard WordPress Post Type Framework
Plugin URI: https://bitbucket.org/estrnstd/eastern-standard-wordpress-post-type-framework
Description: A framework for configuring custom post types.
Version: 2.2.0
Author: Eastern Standard
Author URI: https://easternstandard.com
Update URI: eswp-post-type-framework
*/

require_once(ABSPATH . 'wp-admin/includes/plugin.php');

if (!class_exists('ESWP_Post_Type_Framework')) {
	#[AllowDynamicProperties]
	class ESWP_Post_Type_Framework {

		/**
		 * normalize_post_type_directories
		 * 
		 * Normalize post type directories.
		 * 
		 * @param   string|array  $post_type_directories  Required. Post type directories to be normalized.
		 * @return  $post_type_directories  The normalized post type directories.
		 */
		
		public static function normalize_post_type_directories($post_type_directories) {

			$input_valid = true;

			// Normalize input to an array of strings.
			if (is_string($post_type_directories) || is_array($post_type_directories)) {
				if (is_string($post_type_directories)) {
					$post_type_directories = [$post_type_directories];
				}
				if (is_array($post_type_directories)) {
					foreach ($post_type_directories as $post_type_directory) {
						if (!is_string($post_type_directory)) {
							$input_valid = false;
							break;
						}
					}
				}
			}
			else {
				$input_valid = false;
			}

			// Handle invalid input.
			if (!$input_valid) {
				trigger_error(__("Class 'ESWP_Post_Type_Framework', method 'normalize_post_type_directories', parameter: '\$post_type_directories' must be a string or an array of strings."), E_USER_WARNING);
				return null;
			}

			// Handle globs.
			foreach ($post_type_directories as $key => $post_type_directory) {
				if (str_contains($post_type_directory, '*')) {
					array_splice($post_type_directories, $key, 1, glob($post_type_directory));
				}
			}

			// Remove trailing slashes.
			foreach ($post_type_directories as &$post_type_directory) {
				$post_type_directory = rtrim($post_type_directory, '/');
			}

			return $post_type_directories;

		}

		/**
		 * normalize_post_type_data
		 * 
		 * Normalize post type data.
		 * 
		 * @param   string  $post_type_data  Required. Post type data to be normalized.
		 * @return  $post_type_data  The normalized post type data.
		 */

		public static function normalize_post_type_data($post_type_data) {

			if (!is_array($post_type_data)) {
				trigger_error(__("Class 'ESWP_Post_Type_Framework', method 'normalize_post_type_data', parameter: '\$post_type_data' must be an array."), E_USER_WARNING);
			}

			// Normalize legacy key 'key' to key 'post_type'.
			if (array_key_exists('key', $post_type_data)) {
				if (!array_key_exists('post_type', $post_type_data)) {
					$post_type_data['post_type'] = $post_type_data['key'];
				}
			}

			if (!array_key_exists('post_type', $post_type_data)) {
				$post_type_data['post_type'] = null; // Let register_post_type decide how to error handle.
			}

			// Normalize legacy key 'arguments' to key 'args'.
			if (array_key_exists('arguments', $post_type_data)) {
				if (!array_key_exists('args', $post_type_data)) {
					$post_type_data['args'] = $post_type_data['arguments'];
				}
			}

			if (!array_key_exists('args', $post_type_data)) {
				$post_type_data['args'] = null; // Let register_post_type decide how to error handle.
			}

			// Normalize legacy key 'single-template' to key 'single_template'.
			if (array_key_exists('single-template', $post_type_data)) {
				if (!array_key_exists('single_template', $post_type_data)) {
					$post_type_data['single_template'] = $post_type_data['single-template'];
				}
			}

			return $post_type_data;

		}

		/**
		 * initialize_post_type
		 * 
		 * Registers a post type.
		 * 
		 * @param   string  $post_type_directory  Required. A path to a post type directory.
		 * @return  void
		 */

		public static function initialize_post_type($post_type_directory) {
			$post_type_config_path = $post_type_directory . '/config.php';
			$post_type_data = include $post_type_config_path;
			$post_type_data = self::normalize_post_type_data($post_type_data);
			register_post_type($post_type_data['post_type'], $post_type_data['args']);
			if (isset($post_type_data['single_template']) && is_string($post_type_data['single_template'])) {
				add_filter('single_template', function ($single_template) use($post_type_data) {
					$queried_object = get_queried_object();
					$post_type = $queried_object->post_type;
					if ($post_type === $post_type_data['post_type']) {
						return $post_type_data['single_template'];
					}
					else {
						return $single_template;
					}
				}, 10, 1);
			}
		}

		/**
		 * initialize_post_types
		 * 
		 * Registers post types.
		 * 
		 * @param   mixed  $post_type_directories  Required. Paths to post type directories.
		 * @return  void
		 */

		public static function initialize_post_types($post_type_directories) {
			$post_type_directories = self::normalize_post_type_directories($post_type_directories);
			foreach ($post_type_directories as $post_type_directory) {
				self::initialize_post_type($post_type_directory);
			}
		}

		public function __construct() {

			$this->file = __FILE__;
			$this->dir = __DIR__;
			$this->plugin_data = get_plugin_data(__FILE__);
			$this->slug = plugin_basename(__DIR__);
			$this->basename_path = $this->slug . '/' . $this->slug . '.php';
			$this->dashboard_updating = true;
			$this->info_url = 'https://bitbucket.org/estrnstd/eastern-standard-wordpress-post-type-framework/raw/v2/info.json';
			$this->info_cache_allowed = true;
			$this->info_cache_key = $this->slug . '-info';
			$this->info_cache_length = DAY_IN_SECONDS;

			if (class_exists('ESWP_Updater')) {
				ESWP_Updater::initialize_plugin_updates($this);
			}
			else {
				add_action('plugin-eswp-updater:initialized', function() {
					ESWP_Updater::initialize_plugin_updates($this);
				});
			}

		}

	}

	new ESWP_Post_Type_Framework();

}
