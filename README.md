# Eastern Standard WordPress Post Type Framework
A WordPress plugin to standardize working with post types.

## Installation

Copy the contents of the `eswp-post-type-framework` directory from this repository into your site's plugins directory. The path should look like this: `/wp-content/plugins/eswp-post-type-framework/eswp-post-type-framework.php`.

Activate the plugin via the WordPress dashboard 'Plugins' page (`/wp-admin/plugins.php`).

## Updates
This plugin can be updated from the WordPress dashboard, read about it in the "Updating Plugins" section of the updater plugin readme: <a href="https://bitbucket.org/estrnstd/eastern-standard-wordpress-updater/src/v1" target="_blank">Eastern Standard WordPress Updater</a> (you will need access to view this repository).

## Disabling Updates

This plugin can have it's dashboard updates disabled, read about it in the "Disabling Plugin Updates" section of the updater plugin readme: <a href="https://bitbucket.org/estrnstd/eastern-standard-wordpress-updater/src/v1" target="_blank">Eastern Standard WordPress Updater</a> (you will need access to view this repository).

## Usage

Using the `init` hook, call the `ESWP_Post_Type_Framework::initialize_post_types` method passing in a directory or array of directories containing a custom post type `config.php` file. The `initialize_post_types` method accepts `*` wild cards within the argument strings.
```php
add_action('init', function () {
	if (class_exists('ESWP_Post_Type_Framework')) {
		ESWP_Post_Type_Framework::initialize_post_types(get_template_directory() . '/custom-post-types/*');
	}
	else {
		trigger_error(__("Class 'ESWP_Post_Type_Framework' does not exist. Make sure the 'Eastern Standard WordPress Post Type Framework' plugin is installed and activated."), E_USER_WARNING);
	}
});
```

### Creating Post Types

Included in this repository is an example post type. You can find it located in the `'example-post-type'` directory. It's a good reference for a basic post type, but keep in mind: for existing projects, it is **strongly recommended** that you follow any conventions present in the project you're working in. It might be easier to copy an existing post type rather than start from this example.

The only required file is `config.php`. It should be located directly within any directory path passed to the `initialize_post_types` method. This config file should return an array which will inform the plugin of all the post type configuration data.

The `'post_type'` and `'args'` keys will be passed into the `register_post_type` function. You can read more about the `register_post_type` function here: <a href="https://developer.wordpress.org/reference/functions/register_post_type" target="_blank">https://developer.wordpress.org/reference/functions/register_post_type</a>.

Optionally, a custom single post template for the post type can be defined by setting an absolute path to the template file as the `'single_template'` key.

#### Troubleshooting

**Note:** If after creating or updating a post type, the post single pages returns a 404 error, visit the permalinks settings page in the dashboard and save the page. This often fixes the issue.

## Development

The versioning for this plugin follows the <a href="https://semver.org" target="_blank">semver</a> guidelines.

When committing updates, remember to update the version in `eswp-post-type-framework.php` and `info.json`, then compress the `eswp-post-type-framework` directory into a new `eswp-post-type-framework.zip` file. These steps ensure the WordPress plugin dashboard update process will work as expected.